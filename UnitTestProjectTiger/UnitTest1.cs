using System.Web;
using HubTigerWebApp.Models.Bicycle;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
namespace UnitTestProjectTiger
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            // Arrange
            var bicycle = new BicycleModel();

            // Act
            

            // Assert
            Assert.IsFalse(result);
            var error = _modelState["FirstName"].Errors[0];
            Assert.AreEqual("First name is required.", error.ErrorMessage);
        }
    }
}
