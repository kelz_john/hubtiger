using HubTigerWebApp.Controllers.BicycleController;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    
    [TestClass]
    public class BicycleControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            BicycleModelsController bicycleModelsController = new BicycleModelsController();
           

            // Act
            ViewResult result = bicycleModelsController.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Home Page", result.ViewBag.Title);
        }
    }

}
