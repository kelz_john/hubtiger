﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
namespace HubTigerWebApp.Models.Bicycle
{
    public class BicycleModel
    {
        public int Id { get; set; }
        [Required]
        public string NickName { get; set; }

        public string Manufacturer { get; set; }
        [Required]
        public string ModelYear { get; set; }
        public string Colour { get; set; }
    }
}
