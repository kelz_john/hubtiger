﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using HubTigerWebApp.Models.Bicycle;

namespace HubTigerWebApp.Models
{
    public class HubTigerWebAppContext : DbContext
    {
        public HubTigerWebAppContext (DbContextOptions<HubTigerWebAppContext> options)
            : base(options)
        {
        }

        public DbSet<HubTigerWebApp.Models.Bicycle.BicycleModel> BicycleModel { get; set; }
    }
}
