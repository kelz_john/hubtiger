﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HubTigerWebApp.Models;
using HubTigerWebApp.Models.Bicycle;

namespace HubTigerWebApp.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class BicycleModelsController : ControllerBase
    {
        private readonly HubTigerWebAppContext _context;

        public BicycleModelsController(HubTigerWebAppContext context)
        {
            _context = context;
        }

        

        // GET: api/BicycleModels
        [HttpGet]
        public IEnumerable<BicycleModel> GetBicycleModel()
        {
            return _context.BicycleModel;
        }

        // GET: api/BicycleModels/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBicycleModel([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bicycleModel = await _context.BicycleModel.FindAsync(id);

            if (bicycleModel == null)
            {
                return NotFound();
            }

            return Ok(bicycleModel);
        }

        // PUT: api/BicycleModels/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBicycleModel([FromRoute] int id, [FromBody] BicycleModel bicycleModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bicycleModel.Id)
            {
                return BadRequest();
            }

            _context.Entry(bicycleModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BicycleModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(bicycleModel);
        }

        // POST: api/BicycleModels
        [HttpPost]
        public async Task<IActionResult> PostBicycleModel([FromBody] BicycleModel bicycleModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.BicycleModel.Add(bicycleModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBicycleModel", new { id = bicycleModel.Id }, bicycleModel);
        }

        // DELETE: api/BicycleModels/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBicycleModel([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bicycleModel = await _context.BicycleModel.FindAsync(id);
            if (bicycleModel == null)
            {
                return NotFound();
            }

            _context.BicycleModel.Remove(bicycleModel);
            await _context.SaveChangesAsync();

            return Ok(bicycleModel);
        }

        private bool BicycleModelExists(int id)
        {
            return _context.BicycleModel.Any(e => e.Id == id);
        }
    }
}