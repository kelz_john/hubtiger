﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HubTigerWebApp.Models;
using HubTigerWebApp.Models.Bicycle;

namespace HubTigerWebApp.Controllers.BicycleController
{
    public class BicycleModelsController : Controller
    {

        private readonly HubTigerWebAppContext _context;

        public BicycleModelsController(HubTigerWebAppContext context)
        {
            _context = context;
        }

        // GET: BicycleModels
        public async Task<IActionResult> Index()
        {
            return View(await _context.BicycleModel.ToListAsync());
        }

        // GET: BicycleModels/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bicycleModel = await _context.BicycleModel
                .FirstOrDefaultAsync(m => m.Id == id);
            if (bicycleModel == null)
            {
                return NotFound();
            }

            return View(bicycleModel);
        }

        // GET: BicycleModels/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: BicycleModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,NickName,Manufacturer,ModelYear,Colour")] BicycleModel bicycleModel)
        {
            if (ModelState.IsValid)
            {
                _context.Add(bicycleModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(bicycleModel);
        }

        // GET: BicycleModels/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bicycleModel = await _context.BicycleModel.FindAsync(id);
            if (bicycleModel == null)
            {
                return NotFound();
            }
            return View(bicycleModel);
        }

        // POST: BicycleModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,NickName,Manufacturer,ModelYear,Colour")] BicycleModel bicycleModel)
        {
            if (id != bicycleModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bicycleModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BicycleModelExists(bicycleModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(bicycleModel);
        }

        // GET: BicycleModels/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bicycleModel = await _context.BicycleModel
                .FirstOrDefaultAsync(m => m.Id == id);
            if (bicycleModel == null)
            {
                return NotFound();
            }

            return View(bicycleModel);
        }

        // POST: BicycleModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var bicycleModel = await _context.BicycleModel.FindAsync(id);
            _context.BicycleModel.Remove(bicycleModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BicycleModelExists(int id)
        {
            return _context.BicycleModel.Any(e => e.Id == id);
        }
    }
}
