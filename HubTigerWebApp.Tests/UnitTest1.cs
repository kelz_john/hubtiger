using HubTigerWebApp.Controllers.BicycleController;
using HubTigerWebApp.Models;
using HubTigerWebApp.Models.Bicycle;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HubTigerWebApp.Tests
{
    [TestClass]
    public class UnitTest1
    {
        public HubTigerWebAppContext _context;
        [TestMethod]
        public void BicycleModelCreateTestMethod()
        {
            BicycleModel bicycleModel = new BicycleModel
            {
                NickName = "Test Bicycle",
                Manufacturer = "Test Manufacturer",
                Colour = "Test Colour",
                ModelYear = "Test Year"
            };
            // Arrange
            BicycleModelsController controller = new BicycleModelsController(_context);
            // Act
            var result = controller.Create(bicycleModel);
            // Assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void BicycleModelDeleteTestMethod()
        {
            int deleteId = 3;
            // Arrange
            BicycleModelsController controller = new BicycleModelsController(_context);
            // Act
            var result = controller.Index();
            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void BicycleModelUpdateTestMethod()
        {
            BicycleModel bicycleModel = new BicycleModel
            {
                NickName = "Test Bicycle 1",
                Manufacturer = "Test Manufacturer 1",
                Colour = "Test Colour 1",
                ModelYear = "Test Year 1"
            };
            int editId = 2;
            // Arrange
            BicycleModelsController controller = new BicycleModelsController(_context);
            // Act
            var result = controller.Edit(editId, bicycleModel);
            // Assert
            Assert.IsNotNull(result);
        }

    }
}
