﻿// <auto-generated />
using HubTiger_API.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace HubTiger_API.Migrations
{
    [DbContext(typeof(HubTiger_APIContext))]
    partial class HubTiger_APIContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("HubTiger_API.Model.BicycleModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Colour");

                    b.Property<string>("Manufacturer");

                    b.Property<string>("ModelYear")
                        .IsRequired();

                    b.Property<string>("NickName")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("BicycleModel");
                });
#pragma warning restore 612, 618
        }
    }
}
