﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using HubTiger_API.Model;

namespace HubTiger_API.Models
{
    public class HubTiger_APIContext : DbContext
    {
        public HubTiger_APIContext (DbContextOptions<HubTiger_APIContext> options)
            : base(options)
        {
        }

        public DbSet<HubTiger_API.Model.BicycleModel> BicycleModel { get; set; }
    }
}
