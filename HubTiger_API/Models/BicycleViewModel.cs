﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HubTiger_API.Models
{
    public class BicycleViewModel
    {
        [Required]
        public string NickName { get; set; }
        public string Manufacturer { get; set; }
        [Required]
        public string ModelYear { get; set; }
        public string Colour { get; set; }
    }
}
