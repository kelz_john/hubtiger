using HubTigerWebApp.Controllers.BicycleController;
using HubTigerWebApp.Models.Bicycle;
using Microsoft.AspNetCore.Mvc;
using System;
using Xunit;

namespace XUnitTestProjectTiger
{
    public class UnitTest1
    {
        private BicycleModel bicycle;
        [Fact]
        public async void Task_GetPostById_Return_OkResult()
        {
            //Arrange  
            var controller = new BicycleModelsController();
            int postId = 2;

            //Act  
            var data = await controller.Details(postId);

            //Assert  
            Assert.IsType<OkObjectResult>(data);
        }
    }
}
